const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const productRouter = require("./src/routes/ProductRouter");
const dotEnv = require("dotenv");

// Body using vietnamese
app.use(
  express.urlencoded({
    extended: true,
  })
);
// Config DotEnv
dotEnv.config();
// body json
app.use(express.json());

// Cross Origin Resource Sharing
app.use(cors());

// Connect MongoDB
mongoose
  .connect(process.env.MONGO_DB)
  .then(() => console.log("Connect MongoDB Successfully!"))
  .catch((err) => {
    console.log(err);
  });
app.get("/", (req, res) => {
  res.json({
    message: "MY FOOD API",
  });
});

app.use("/api/products", productRouter);
// Start Server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
