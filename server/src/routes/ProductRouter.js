const express = require("express");

const {
  createProduct,
  getAllProduct,
  getCountTotalProducts,
  getSingleProduct,
} = require("../controller/ProductController");

const router = express.Router();

router.post("/", createProduct);
router.get("/", getAllProduct);
router.get("/count", getCountTotalProducts);
router.get("/:productId", getSingleProduct);

module.exports = router;
