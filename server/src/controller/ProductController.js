const ProductModel = require("../models/Product");

const createProduct = (req, res) => {
  const newProduct = new ProductModel(req.body);
  newProduct
    .save()
    .then((newProduct) => {
      return res.status(200).json({
        message: "Success",
        product: newProduct,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

const getAllProduct = (req, res) => {
  let { page, limit, title } = req.query;
  if (!title) {
    title = {};
  }
  if (page) {
    // Get PAGE
    let pageIsNumber = parseInt(page); //String to Number , Query is string
    let skip = (pageIsNumber - 1) * limit;
    ProductModel.find()
      .skip(skip)
      .limit(limit)
      .then((productList) => {
        return res.status(200).json(productList);
      })
      .catch((err) => {
        return res.status(500).json({
          message: "Fail",
          error: err.message,
        });
      });
  } else {
    // Get All
    ProductModel.find(title)
      .then((productList) => {
        return res.status(200).json(productList);
      })
      .catch((err) => {
        return res.status(500).json({
          message: "Fail",
          error: err.message,
        });
      });
  }
};

const getSingleProduct = (req, res) => {
  ProductModel.findOne({ _id: req.params.productId })
    .then((product) => {
      return res.status(200).json(product);
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

function getCountTotalProducts(req, res) {
  ProductModel.count()
    .then((number) => {
      return res.status(200).json(number);
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

module.exports = {
  createProduct,
  getAllProduct,
  getCountTotalProducts,
  getSingleProduct
};
