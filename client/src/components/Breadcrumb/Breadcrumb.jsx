import React from "react";
import { Link, useLocation } from "react-router-dom";
import styles from "./Breadcrumb.module.css";
import { AiFillHome } from "react-icons/ai";

const Breadcrumb = () => {
  const location = useLocation();
  const pathnames = location.pathname.split("/").filter((x) => x);
  return (
    <>
      {pathnames.length !== 0 ? (
        <ul className={styles.breadcrumb}>
          <li className={styles.breadcrumbItem}>
            <Link to="/">
              <AiFillHome /> Home
            </Link>
          </li>
          {pathnames.map((path, index) => {
            return (
              <li key={index} className={styles.breadcrumbItem}>
                <Link to={`/${path}`}>{path}</Link>
              </li>
            );
          })}
        </ul>
      ) : null}
    </>
  );
};

export default Breadcrumb;
