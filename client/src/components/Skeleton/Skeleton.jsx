import React from "react";
import "./Skeleton.css";

const Skeleton = ({ className = "" }) => {
  // destructuring
  return <div className={`skeleton ${className}`}></div>;
};

export default Skeleton;
