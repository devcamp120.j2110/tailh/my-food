import styles from "./Card.module.css";
import { numberWithCommas } from "../../utils";
import { array } from "prop-types";
import { navigateToProductDetail } from "~/utils";
import { useNavigate } from "react-router-dom";
import Skeleton from "../Skeleton";

const Card = ({ products }) => {
  const navigate = useNavigate();
  return (
    <>
      {/* Card Item */}
      {products.map((card) => {
        return (
          <div key={card._id} className={styles.cardItem}>
            {/* Img */}
            <div
              onClick={() => {
                navigateToProductDetail(navigate, card._id);
              }}
              className={styles.cardItemImg}
            >
              <img src={card.img} alt="cart-item" />
            </div>
            {/* Content */}
            <div className={styles.cardItemContent}>
              {/* Title */}
              <h5 className={styles.cardItemTitle}>{card.title}</h5>
              {/* Desc */}
              <p className={styles.cardItemDesc}>{card.desc}</p>
              {/* Footer */}
              <div className={styles.cardItemFooter}>
                <button className={styles.cardItemBtn}>Add To Cart</button>
                <h3 className={styles.cardItemPrice}>
                  ${numberWithCommas(card.price)}
                </h3>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

const Loading = () => {
  return (
    <>
      {/* Card Item */}
      {Array(4)
        .fill(0)
        .map((_, index) => {
          return (
            <div key={index} className={styles.cardItem}>
              {/* Img */}
              <div className={styles.cardItemImg}>
                <Skeleton className="fullView" />
              </div>
              {/* Content */}
              <div
                className={`${styles.cardItemContent} ${styles.cardItemContentLoading}`}
              >
                {/* Title */}
                <Skeleton className="fullView title small" />
                {/* Desc */}
                <Skeleton className="fullView paragraph" />
                {/* Footer */}
                <Skeleton className="fullView button" />
              </div>
            </div>
          );
        })}
    </>
  );
};

Card.Loading = Loading;

Card.defaultProps = {
  products: [],
};

Card.prototype = {
  products: array.isRequired,
};

export default Card;
