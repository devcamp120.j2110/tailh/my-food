import React from "react";
import styles from "./Header.module.css";
// Icon
import { FaSearch, FaUser, FaShoppingCart } from "react-icons/fa";
import { GiMeat } from "react-icons/gi";
import { BiStore } from "react-icons/bi";
import { MdDashboard } from "react-icons/md";
import { useNavigate } from "react-router-dom";

const Header = () => {
  let navigate = useNavigate();
  const PAGE = {
    home: "HOME",
    cart: "CART",
    about: "ABOUT",
    login: "LOGIN",
  };
  /**
   *
   * @param {Page} paramPage //Navigate tương ứng với page
   */
  const navigatePage = (paramPage) => {
    switch (paramPage) {
      case PAGE.home:
        navigate("/");
        break;
      case PAGE.cart:
        navigate("/cart");
        break;
      case PAGE.about:
        navigate("/about");
        break;
      case PAGE.login:
        navigate("/login");
        break;
      default:
        return;
    }
  };

  return (
    <div className={`${styles.header} container-fluid`}>
      <div className={`${styles.headerContainer} container`}>
        {/* Logo Desktop */}
        <div
          onClick={() => {
            navigatePage(PAGE.home);
          }}
          className={styles.logo}
        >
          My Food <BiStore />
        </div>
        {/* Logo Mobile */}
        <div className={styles.mobileLogo}>
          <BiStore />
        </div>
        {/* Search */}
        <div className={styles.search}>
          <input type="text" placeholder="Search food here..." />
          <FaSearch />
        </div>
        {/* Mobile Shop Name */}
        <div className={styles.mobileShopName}>
          <h1>My Food</h1>
        </div>

        {/* Desktop Navigation Group */}
        <div className={styles.navGroup}>
          {/* Notification */}
          <div
            onClick={() => {
              navigatePage(PAGE.about);
            }}
            className={styles.navItem}
          >
            <div className={`${styles.navIcon} ${styles.aboutUsIcon}`}>
              <GiMeat />
            </div>
            <p className={styles.navDesc}>About Us</p>
          </div>
          {/* Cart */}
          <div
            onClick={() => {
              navigatePage(PAGE.cart);
            }}
            className={styles.navItem}
          >
            <div className={styles.navIcon}>
              <FaShoppingCart />
            </div>
            <p className={styles.navDesc}>Cart</p>
          </div>
          {/* User */}
          <div onClick={() => {navigatePage(PAGE.login)}}className={styles.navItem}>
            <div className={styles.navIcon}>
              <FaUser />
            </div>
            <p className={`${styles.navDesc} ${styles.active}`}>Sign In</p>
          </div>
        </div>
        {/* Mobile Navigation Group */}
        <div className={styles.mobileNavGroup}>
          <div className={styles.mobileNavIcon}>
            <MdDashboard />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
