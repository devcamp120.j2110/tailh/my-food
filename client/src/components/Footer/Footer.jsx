import React from "react";
// Style
import styles from "./Footer.module.css";
// Icon
import { BiStore } from "react-icons/bi";
import { FaDribbble, FaFacebookSquare, FaPinterest } from "react-icons/fa";
import { BsTwitter } from "react-icons/bs";
import { AiOutlineInstagram } from "react-icons/ai";
import { GiRotaryPhone } from "react-icons/gi";
import { MdLocationOn, MdAccessTimeFilled, MdEmail } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { navigateToHomePage } from "~/utils";

const Footer = () => {
  let navigate = useNavigate();

  return (
    <div className={`${styles.footer} container-fluid`}>
      <div className={`${styles.footerContainer} container`}>
        {/* Footer Top */}
        <div className={styles.footerTop}>
          {/* Content Left */}
          <div className={styles.contentLeft}>
            <div
              onClick={() => {
                navigateToHomePage(navigate);
              }}
              className={styles.logo}
            >
              My Food <BiStore />
            </div>
            <p className={styles.desc}>
              My food is the world's leading
              <br /> community for food , quanlity, grow,
              <br /> and get hired.
            </p>
            <div className={styles.iconGroup}>
              <div className={styles.icon}>
                <FaDribbble />
              </div>
              <div className={styles.icon}>
                <BsTwitter />
              </div>
              <div className={styles.icon}>
                <FaFacebookSquare />
              </div>
              <div className={styles.icon}>
                <AiOutlineInstagram />
              </div>
              <div className={styles.icon}>
                <FaPinterest />
              </div>
            </div>
          </div>
          {/* End Content Left */}
          {/* Content Right */}
          <div className={styles.contentRight}>
            <ul>
              <li>
                <h5 className={styles.title}>Foods</h5>
              </li>
              <li>Vietnamese Foods</li>
              <li>Thailand Foods</li>
              <li>Chinese Foods</li>
            </ul>
            <ul>
              <li>
                <h5 className={styles.title}>Topics</h5>
              </li>
              <li>Recipe</li>
              <li>Reviews</li>
              <li>Comments</li>
              <li>Favorite</li>
            </ul>
            <ul>
              <li>
                <h5 className={styles.title}>Company</h5>
              </li>
              <li>Delivery</li>
              <li>Prices</li>
              <li>Best seller</li>
              <li>Contact Us</li>
            </ul>
            <ul>
              <li>
                <h5 className={styles.title}>Infomation</h5>
              </li>
              <li>
                <MdLocationOn /> takayama , konoha
              </li>
              <li>
                <GiRotaryPhone /> 123456789
              </li>
              <li>
                <MdAccessTimeFilled /> 7 A.M - 5 P.M
              </li>
              <li>
                <MdEmail /> email123@gmail.com
              </li>
            </ul>
          </div>
          {/* End Content Right */}
        </div>
        {/* Footer Bottom */}
        <div className={styles.footerBottom}>
          <p className={styles.copyRight}>
            Copyright 2022 My Food, Inc. All Rights Reserved
          </p>
          <p className={styles.career}>Careeries</p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
