import styles from "../Pagination/Pagination.module.css";
import { GrFormPreviousLink, GrFormNextLink } from "react-icons/gr";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { autoScrollTop } from "../../utils";
const Pagination = (props) => {
  const totalItems = useSelector((state) => state.products.counters);
  const [totalPage, setTotalPage] = useState(0);
  const totalPages = Math.ceil(totalItems / props.pagination.limit);

  const onPrevButtonClick = () => {
    if (props.pagination.page > 1) {
      props.setPagination({
        ...props.pagination,
        page: props.pagination.page - 1,
      });
    }
  };

  const onNextButtonClick = () => {
    if (props.pagination.page < totalPage) {
      props.setPagination({
        ...props.pagination,
        page: props.pagination.page + 1,
      });
    }
  };

  const onPaginationNumberClick = (pageNumber) => {
    props.setPagination({
      ...props.pagination,
      page: pageNumber,
    });
  };

  useEffect(() => {
    setTotalPage(totalPages);
  }, [totalItems, totalPages]);

  useEffect(() => {
    autoScrollTop();
  }, []);
  return (
    <div className={styles.pagination}>
      <ul>
        <li onClick={onPrevButtonClick} className={styles.prevBtn}>
          <GrFormPreviousLink />
          <span>Prev</span>
        </li>
        {[...Array(totalPage)].map((ele, index) => {
          const active =
            index + 1 === props.pagination.page ? styles.active : "";
          return (
            <li
              onClick={() => {
                onPaginationNumberClick(index + 1);
              }}
              className={`${styles.pagiNumber} ${active}`}
              key={index}
            >
              <span>{index + 1}</span>
            </li>
          );
        })}
        <li onClick={onNextButtonClick} className={styles.nextBtn}>
          <span>Next</span>
          <GrFormNextLink />
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
