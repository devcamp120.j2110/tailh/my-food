import React from "react";
import styles from "./Chip.module.css";
import PropTypes from "prop-types";

const Chip = (props) => {
  const { size, children, ...otherProps } = props;

  return (
    <div
      className={`${styles.chipContainer} ${styles[size]}`}
      // Rest Props
      {...otherProps}
    >
      {children}
    </div>
  );
};

Chip.defaultProps = {
  size: "medium",
};

Chip.prototype = {
  size: PropTypes.oneOf(["small", "medium", "large"]),
};

export default Chip;
