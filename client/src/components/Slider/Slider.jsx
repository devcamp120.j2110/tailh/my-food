import React from "react";
// Import Swiper React components
import { Swiper as SlideContainer, SwiperSlide as Slide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import styles from "./Slide.module.css";
import { banner } from "../../utils";
// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";
const Slider = () => {
  return (
    <SlideContainer
      style={{
        "--swiper-navigation-color": "#fff",
        "--swiper-pagination-color": "#fff",
      }}
      slidesPerView={1}
      spaceBetween={30}
      loop={true}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Pagination, Autoplay, Navigation]}
      autoplay={{
        delay: 2500,
        disableOnInteraction: false,
      }}
      className={styles.slideContainer}
    >
      {banner.map((img, index) => {
        return (
          <Slide key={index}>
            <div className={styles.bannerWrapper}>
              <img src={img} alt="banner-img" />
            </div>
          </Slide>
        );
      })}
    </SlideContainer>
  );
};

export default Slider;
