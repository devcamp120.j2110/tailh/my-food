import { FaPlus, FaMinus } from "react-icons/fa"
import { CTooltip } from "@coreui/react"
import { useRef, useState, useEffect, memo } from "react"
import { Translation } from "react-i18next"
import { bool, string, oneOf, number } from "prop-types"

const PSCShortenString = (props) => {
    const _isMounted = useRef(false)

    const { value, expand, mode } = props
    const [isExpand, setIsExpand] = useState(expand ?? true)
    const [isShowExpand, setIsShowExpand] = useState(false)

    const length = props.length ?? 100
    const languageResource = {
        GeneralConfiguration_Expand: "Mở rộng",
        GeneralConfiguration_Shorten: "Rút gọn",
    }

    useEffect(() => {
        _isMounted.current = true
        return () => {
            _isMounted.current = false
        }
    }, [])

    useEffect(() => {
        _isMounted.current && setIsExpand(expand)
    }, [expand])

    if (value?.length < length || !value?.length > 0) return value

    const text = value.slice(0, length)

    switch (mode) {
        case "hover":
            return <CTooltip content={value} children={<>{text}...</>} />
        case "button":
            return <span>
                        {isExpand ? value : text}
                        <CTooltip
                            {...isExpand
                                ? {
                                    content: languageResource.GeneralConfiguration_Shorten,
                                    children: <><FaMinus className="btn btn-link" onClick={() => _isMounted.current && setIsExpand(false)} size="1rem" /></>,
                                }
                                : {
                                    content: languageResource.GeneralConfiguration_Expand,
                                    children: <>...<FaPlus className="btn btn-link" onClick={() => _isMounted.current && setIsExpand(true)} size="1rem" /></>,
                                }
                            }
                        />
                    </span>
        case "expandOnly":
        default:
            if (isShowExpand) return value

            return <span>
                        {text}
                        <CTooltip
                            content={languageResource.GeneralConfiguration_Expand}
                            children={<>...<FaPlus className="btn btn-link" onClick={() => _isMounted.current && setIsShowExpand(true)} size="1rem" /></>}
                        />
                    </span>
    }
}

PSCShortenString.prototype = {
    value: string,
    mode: oneOf(["hover", "button", "expandOnly"]),
    length: number,
    expand: bool,
}

export default memo(PSCShortenString)