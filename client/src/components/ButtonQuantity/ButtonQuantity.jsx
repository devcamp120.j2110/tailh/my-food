import React from "react";
import styles from "./ButtonQuantity.module.css";
import { HiPlus, HiMinus } from "react-icons/hi";
import PropTypes from "prop-types";

const ButtonQuantity = (props) => {
  const { size, type, ...otherProps } = props;
  const renderBodyButton = () => {
    switch (type) {
      case "increase":
        return <HiPlus />;
      case "decrease":
        return <HiMinus />;
      default:
        break;
    }
  };
  return (
    <div
      className={`${styles.buttonQuantityContainer} ${styles[size]}`}
      // Rest Props
      {...otherProps}
    >
      {renderBodyButton()}
    </div>
  );
};

ButtonQuantity.defaultProps = {
  size: "medium",
  type: "increase",
};

ButtonQuantity.prototype = {
  size: PropTypes.oneOf(["small", "medium", "large"]),
  type: PropTypes.oneOf(["increase", "decrease"]).isRequired,
};

export default ButtonQuantity;
