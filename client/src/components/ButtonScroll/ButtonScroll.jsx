import React, { useEffect, useState } from "react";

const ButtonScroll = () => {
  const [btnScroll, setBtnScroll] = useState({
    top: true,
    bottom: false,
  });

  const onScrollChangeStatusBtn = () => {
    let viewportHeight = window.innerHeight;
    let contentHeight = document.documentElement.scrollHeight;
    let viewableRatio = contentHeight / viewportHeight;
    let thumbHeight = viewportHeight * viewableRatio;
    let endPoint = Math.abs(viewportHeight - thumbHeight);
    console.log(endPoint);
    console.log(window.scrollY);

    if (
      window.scrollY > Math.round(endPoint * 0.1) &&
      window.scrollY < Math.round(endPoint * 0.4)
    ) {
      console.log("Top");
      setBtnScroll({
        top: false,
        bottom: true,
      });
    } else if (
      window.scrollY > Math.round(endPoint * 0.4) &&
      window.scrollY < Math.round(endPoint * 0.6)
    ) {
      console.log("Middle");
      setBtnScroll({
        top: false,
        bottom: false,
      });
    } else if (window.scrollY > Math.round(endPoint * 0.8)) {
      console.log("Bottom");
      setBtnScroll({
        top: true,
        bottom: false,
      });
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", onScrollChangeStatusBtn);
    return () => {
      window.removeEventListener("scroll", onScrollChangeStatusBtn);
    };
  }, []);

  return (
    <div style={{ position: "fixed", right: 0, bottom: "10%" }}>
      <button disabled={btnScroll.top}>GO TOP</button>
      <button disabled={btnScroll.bottom}>GO BOTTOM</button>
    </div>
  );
};

export default ButtonScroll;
