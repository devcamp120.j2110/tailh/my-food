import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  list: [],
  singleItem: null,
  counters: 0,
};

export const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    getAllProduct: (state, action) => {
      state.list = action.payload;
    },
    getSingleProduct: (state, action) => {
      state.singleItem = action.payload;
    },
    getCountProduct: (state, action) => {
      state.counters = action.payload;
    },
  },
});

export const { getAllProduct, getCountProduct, getSingleProduct } = productSlice.actions;

export default productSlice.reducer;
