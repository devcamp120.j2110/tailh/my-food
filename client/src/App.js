import { Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import DefaultLayout from "./layout/DefaultLayout";
import Error from "./page/Error";
import { publicRoutes } from "./routes";
import { Fragment } from "react";
import Loading from "./components/Loading";


function App() {
  return (
    <Suspense fallback={<Loading />}>
      <Routes>
        {/* Public Routes with Default Layout */}
        {publicRoutes &&
          publicRoutes.map((route, index) => {
            // Check Layout
            const Layout = route.layout === null ? Fragment : DefaultLayout;
            // Get Page
            const Page = route.component;
            return (
              <Route
                key={index}
                exact
                path={route.path}
                element={
                  <Layout>
                    <Page />
                  </Layout>
                }
              />
            );
          })}
        {/* 404 Page */}
        <Route exact path="*" element={<Error />} />
      </Routes>
    </Suspense>
  );
}

export default App;
