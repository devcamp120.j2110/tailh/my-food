// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCnjy2zkhlLWWORlbCxD09sv40zZ2AbAEs",
  authDomain: "my-food-17749.firebaseapp.com",
  projectId: "my-food-17749",
  storageBucket: "my-food-17749.appspot.com",
  messagingSenderId: "155679352552",
  appId: "1:155679352552:web:1ef0537284938f04e18b5d"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);