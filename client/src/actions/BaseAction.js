import PropTypes from 'prop-types';

class BaseActions {
  constructor(props) {
    this.service = props.service
    this.dispatchFunc = props.dispatchFunc
  }
  GET_MESSAGE_FAIL_TEXT = "An error occurred when calling Get Method"
  POST_MESSAGE_FAIL_TEXT = "An error occurred when calling POST Method"
  PUT_MESSAGE_FAIL_TEXT = "An error occurred when calling PUT Method"
  DELETE_MESSAGE_FAIL_TEXT = "An error occurred when calling DELETE Method"
  /**
   * mapping object/ array keys if needed
   * @param {*} data Some data to handle mapping
   * @returns new data with correct keys
   */
  _mapping(data) {
    // do some assign here
    return data
  }
  /**
   * Get data 
   * @param {*} pageInfo page information
   * @returns http response
   */
  Get = () => async (dispatch) => {
    try {
      const res = await this.service.Get();

      if (res.status === 200 && res.statusText === "OK") {
        res.data = this._mapping(res.data)
        dispatch?.(this.dispatchFunc.getAll);
      }

      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  GetPage = pagingInfor => async (dispatch) => {
    try {
      const res = await this.service.GetPage(pagingInfor);
      if (res.status === 200 && res.statusText === "OK") {
        res.data = this._mapping(res.data)
        dispatch?.(this.dispatchFunc.getAll(res.data));

        return Promise.resolve(res);
      }

      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  GetSingle = id => async (dispatch) => {
    try {
      const res = await this.service.GetSingle(id);

      if (res.status === 200 && res.statusText === "OK") {
        res.data = this._mapping(res.data)
        dispatch?.(this.dispatchFunc.getSingle(res.data));
      }

      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };
}

BaseActions.propTypes = {
  dispatchFunc: PropTypes.object.isRequired,
  service: PropTypes.any.isRequired,
};

BaseActions.defaultProps = {
};

export default BaseActions;