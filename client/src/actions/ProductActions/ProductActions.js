import ProductService from '~/services/ProductService';
import BaseActions from "../BaseAction";
import {
    getAllProduct,
    getCountProduct,
    getSingleProduct
} from "~/store/features/productSlice";

const actionsProps = {
    dispatchFunc: {
        getAll: getAllProduct,
        getSingle: getSingleProduct
    },
    service: ProductService,
}

class ProductActions extends BaseActions {
    // Set Up Other Dispatch
    otherDispatch = {
        getCountProduct: getCountProduct
    }
    GetCountProduct = () => async (dispatch) => {
        try {
            const res = await this.service.GetCountProduct();

            if (res.status === 200) {
                res.data = this._mapping(res.data)
                dispatch?.(this.otherDispatch.getCountProduct(res.data));
            }

            return Promise.resolve(res);
        } catch (err) {
            return Promise.reject(err);
        }
    };
}
export default new ProductActions(actionsProps);