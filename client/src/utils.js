export const gallary = [
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fgallary1.png?alt=media&token=0a5b39bf-ac66-4a53-b685-1992b1916b15",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fgallary2.png?alt=media&token=2cdc9217-4cc0-4ec9-9ec4-4d1cd63686ac",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fgallary3.png?alt=media&token=d4462463-3e94-494d-8f6c-345b04ef9f2a",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fgallary4.png?alt=media&token=7935a26b-3910-4e31-954e-3b4445c04cf1",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fgallary5.png?alt=media&token=882750ad-43d0-4c00-944d-ea06a5fd0c3b",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fgallary6.png?alt=media&token=be8b595f-38a2-4922-8e35-ba121f5549bd",
];

export const banner = [
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fbanner1.png?alt=media&token=c40fe86b-7e70-4bf4-bd65-737f172437c1",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fbanner2.png?alt=media&token=3e1229aa-8a37-46ae-9459-b460ce373d74",
  "https://firebasestorage.googleapis.com/v0/b/my-food-17749.appspot.com/o/images%2Fbanner3.png?alt=media&token=958c85df-6db8-43a1-887f-7bfd7ea3b6cb",
];

export const numberWithCommas = (number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const autoScrollTop = () => {
  window.scrollTo({ top: 0, behavior: "smooth" });
};

export const deepCloneObject = (object) => {
  return JSON.parse(JSON.stringify(object));
}

// -------------- Navigation -------------- //
export const navigateToHomePage = (navigate) => {
  // Navigate Home Page
  navigate("/");
  // Scroll To Top
  window[`scrollTo`]({ top: 0, behavior: "smooth" });
};

export const navigateTo404Page = (navigate) => {
  // Navigate 404 Page
  navigate("/404");
};

export const navigateToProductDetail = (navigate, id) => {
  // Navigate Product Detail
  navigate(`/products/${id}`);
};