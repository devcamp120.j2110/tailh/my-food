import axios from "axios";
import BaseService from "../BaseService";

class ProductService extends BaseService {
  // Add New Service Inside
  async GetCountProduct() {
    return axios
      .get(`${this.serviceUrl}/count`)
      .then(this.handleResponse)
      .catch(this.handleError);
  }
}

export default new ProductService({ endPoint: "products" });
