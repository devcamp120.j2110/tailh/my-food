import { publicRequest } from "../baseAxios";
import {
  getAllProduct,
  getCountProduct,
} from "../../../store/features/productSlice";

//FUNCTION GET ALL PRODUCTS FROM API
export const getAllProductsFromApi = async (pagination, dispatch) => {
  try {
    const res = await publicRequest.get(
      `products?limit=${pagination.limit}&page=${pagination.page}&title=${pagination.title}`
    );
    if (res.status === 200 && res.statusText === "OK") {
      // Save To Redux Store
      dispatch?.(getAllProduct(res.data.products));
      return res.data.products;
    } else {
      return res;
    }
  } catch (error) {
    console.log(error);
  }
};

//FUNCTION GET COUNT PRODUCTS FROM API
export const getCountProductsFromApi = async (dispatch) => {
  try {
    const res = await publicRequest.get(`products/count`);
    if (res.status === 200 && res.statusText === "OK") {
      // Save To Redux Store
      dispatch?.(getCountProduct(res.data.totalProduct));
      return res.data.totalProduct;
    } else {
      return res;
    }
  } catch (error) {
    console.log(error);
  }
};
