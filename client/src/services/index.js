import ProductService from "./ProductService";

/**all services export */
const services = {
  ...ProductService
}
export default services;