import Configuration from "../configService";
import axios from "axios";
import PropTypes from "prop-types";

class BaseService {
    // config Repository for all API
    constructor(props) {
        this.defaultApiUrl = "http://localhost:8080/"
        this.config = Configuration;
        this.endpoint = props.endPoint;
        this.serviceUrl = `${this.config.REACT_APP_API_URL || this.defaultApiUrl}api/${this.endpoint}`;
    }

    createObj(obj, objNew = {}) {
        if (obj)
            Object.keys(obj).forEach((keyName, i) => {
                objNew[keyName] = obj[keyName] ?? null;
            });

        return objNew;
    }

    async Get(pagingInfor) {
        const parameters = this.createObj(pagingInfor);
        return axios
            .get(this.serviceUrl, { params: parameters })
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async GetPage(pagingInfor) {
        const parameters = this.createObj(pagingInfor);
        return axios
            .get(`${this.serviceUrl}`, { params: parameters })
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async GetSingle(id) {
        return axios
            .get(`${this.serviceUrl}/${id}`)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async Insert(newObj, { isFormData }) {
        if (isFormData) newObj = this.getFormData(isFormData);
        return axios
            .post(`${this.serviceUrl}`, newObj)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async InsertList(items, { isFormData }) {
        if (isFormData) items = this.getFormData(items);
        return axios
            .post(`${this.serviceUrl}/InsertList`, items)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async Delete(id) {
        return axios
            .delete(`${this.serviceUrl}/${id}`)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async DeleteList(items, { isFormData }) {
        if (isFormData) items = this.getFormData(items);
        return axios
            .put(`${this.serviceUrl}/DeleteList`, items)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async Update(id, obj, { isFormData }) {
        if (isFormData) obj = this.getFormData(obj);
        return axios
            .put(`${this.serviceUrl}/${id}`, obj)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async UpdateList(items, { isFormData }) {
        if (isFormData) items = this.getFormData(items);
        return axios
            .put(`${this.serviceUrl}/UpdateList`, items)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async CheckExist(pagingInfor) {
        const parameters = this.createObj(pagingInfor);
        return axios
            .get(`${this.serviceUrl}/CheckExist`, { params: parameters })
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    handleResponse(response) {
        if (response.status === 200 && response.statusText === "OK") {
            return response;
        } else {
            throw new Error(response.status);
        }
    }

    handleError(error) {
        switch (true) {
            case error.response !== undefined && error.response !== null:
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                // error = {...error.response, message: error.response.data ?? "Error"};
                // throw new Error(error.response.status + " - " + error.response.data);
                error = {
                    status: error.response.status,
                    message: error.response.data ?? "HTTP error",
                };
                break;
            case error.request !== undefined && error.request !== null:
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                error = {
                    status: error.request.status,
                    message: error.request.data ?? "HTTP error",
                };
                break;
            case error.message?.length > 0:
                error = {
                    status: error.status ?? error.response?.status ?? 400,
                    message: error.message,
                };
                break;
            default:
                // Something happened in setting up the request that triggered an Error
                error = {
                    status: 400,
                    message: "HTTP error",
                };
                break;
        }

        if (error.message?.startsWith?.("#")) {
            const idx = error.message.indexOf("-");
            if (idx !== -1)
                error.message = error.message.slice(idx + 1, error.message.length);
        } else if (error.message?.startsWith?.("<!DOCTYPE html>")) {
            error.message = "HTTP error";
        }
        return error;
    }

    getFormData(data, parentKey, formData = new FormData()) {

        Object.keys(data).forEach(key => {
            if (typeof data[key] !== 'object') formData.append(key, data[key])
            else formData.append(key, JSON.stringify(data[key]))
        })
        return formData;
    }
}

BaseService.propTypes = {
    endPoint: PropTypes.string.isRequired,
    version: PropTypes.string,
};

export default BaseService;
