// Page
import React from "react";
// Component Import Dynamic
const getDynamicComp = (endPoint, subEndPoint = "") => {
  const temp = React.lazy(() => import(`src/page/${endPoint}/${subEndPoint}`));
  return temp;
};

const component = {
  Home: getDynamicComp("Home"),
  Products: getDynamicComp("Products"),
  ProductDetail: getDynamicComp("ProductDetail"),
  SignUp: getDynamicComp("SignUp"),
  Login: getDynamicComp("Login"),
  Cart: getDynamicComp("Cart"),
  About: getDynamicComp("About"),
  Error: getDynamicComp("Error"),
};

// Public Routes
const publicRoutes = [
  {
    path: "/",
    component: component.Home,
  },
  {
    path: "/sign-up",
    component: component.SignUp,
    layout: null,
  },
  {
    path: "/login",
    component: component.Login,
    layout: null,
  },
  {
    path: "/products",
    component: component.Products,
  },
  {
    path: "/products/:productId",
    component: component.ProductDetail,
  },
  {
    path: "/cart",
    component: component.Cart,
  },
  {
    path: "/about",
    component: component.About,
  },
  {
    path: "/404",
    component: component.Error,
    layout: null,
  },
];

// Private Routes
const privateRoutes = [];

export { publicRoutes, privateRoutes };
