const fakeData = [
  {
    id: 1,
    name: "Bánh canh",
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem quo ducimus, eligendi itaque nostrum suscipit, natus non iste",
    img:"url",
    price: 1000,
    cates: ["north", "noddles"],
    star: 4,
  },
  {
    id: 2,
    name: "Phở",
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem quo ducimus, eligendi itaque nostrum suscipit, natus non iste",
    img:"url",
    price: 1200,
    cates: ["north", "noddles"],
    star: 5,
  },
  {
    id: 3,
    name: "Siêu cấp ramen",
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem quo ducimus, eligendi itaque nostrum suscipit, natus non iste",
    img:"url",
    price: 900,
    cates: ["north", "noddles"],
    star: 4,
  },
  {
    id: 4,
    name: "Bún Real",
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem quo ducimus, eligendi itaque nostrum suscipit, natus non iste",
    img:"url",
    price: 1300,
    cates: ["north", "noddles"],
    star: 5,
  },
];

export default fakeData;
