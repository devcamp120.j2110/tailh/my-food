import { Fragment } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Breadcrumb from "../components/Breadcrumb/Breadcrumb";
// import ButtonScroll from "../components/ButtonScroll/ButtonScroll";

// DefaultLayout
const DefaultLayout = ({ children }) => {
  return (
    <Fragment>
      <Header />
      <div className="container page">
        <Breadcrumb />
        {children}
      </div>
      <Footer />
      {/* <ButtonScroll /> */}
    </Fragment>
  );
};

export default DefaultLayout;
