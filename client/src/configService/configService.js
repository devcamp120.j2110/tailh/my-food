class Configuration {
    // Setup API 
    REACT_APP_API_URL = process.env.REACT_APP_API_URL;
}
export default new Configuration();