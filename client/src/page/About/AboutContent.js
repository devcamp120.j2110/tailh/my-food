// Banner
import aboutBanner1 from "~/assets/images/about-banner1.jpg";
import aboutBanner2 from "~/assets/images/about-banner2.jpg";
import aboutBanner3 from "~/assets/images/about-banner3.jpg";
// Reviewer
import reviewer1 from "~/assets/images/reviewer1.jpg";
import reviewer2 from "~/assets/images/reviewer2.jpg";
import reviewer3 from "~/assets/images/reviewer3.jpg";
import reviewer4 from "~/assets/images/reviewer4.jpg";
const aboutContent = [
  {
    header: "Quality",
    title: "Things that are better in quality are long-lasting",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit.Cupiditate eum ipsa aspernatur minima possimus enim neque doloribus, ratione esse, beatae aliquid. Voluptatum expedita eveniet deleniti. Necessitatibus earum corrupti dolorum! Atque.Lorem, ipsum dolor sit amet consectetur adipisicing elit.Cupiditate eum ipsa aspernatur minima possimus enim neque doloribus, ratione esse, beatae aliquid. Voluptatum expedita eveniet deleniti. Necessitatibus earum corrupti dolorum! Atque.",
    img: aboutBanner1,
  },
  {
    header: "Experience",
    title: "Great stores. Great choices.",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit.Cupiditate eum ipsa aspernatur minima possimus enim neque doloribus, ratione esse, beatae aliquid. Voluptatum expedita eveniet deleniti. Necessitatibus earum corrupti dolorum! Atque. Lorem, ipsum dolor sit amet consectetur adipisicing elit.Cupiditate eum ipsa aspernatur minima possimus enim neque doloribus, ratione esse, beatae aliquid. Voluptatum expedita eveniet deleniti. Necessitatibus earum corrupti dolorum! Atque.",
    img: aboutBanner2,
  },
  {
    header: "Service",
    title: "Your happiness is what matters",
    desc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit.Cupiditate eum ipsa aspernatur minima possimus enim neque doloribus, ratione esse, beatae aliquid. Voluptatum expedita eveniet deleniti. Necessitatibus earum corrupti dolorum! Atque. Lorem, ipsum dolor sit amet consectetur adipisicing elit.Cupiditate eum ipsa aspernatur minima possimus enim neque doloribus, ratione esse, beatae aliquid. Voluptatum expedita eveniet deleniti. Necessitatibus earum corrupti dolorum! Atque.",
    img: aboutBanner3,
  },
];

const reviewerContent = [
  {
    name: "Alex",
    desc: "We Vibe !",
    img: reviewer1,
  },
  {
    name: "Jaim",
    desc: "We Move !",
    img: reviewer2,
  },
  {
    name: "Haley",
    desc: "We Enjoy !",
    img: reviewer3,
  },
  {
    name: "Heena",
    desc: "We Connect !",
    img: reviewer4,
  },
];

export { aboutContent, reviewerContent };
