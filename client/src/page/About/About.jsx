import React from "react";
import styles from "../../styles/About.module.css";
import { aboutContent, reviewerContent } from "./AboutContent";
import aboutBanner from "~/assets/images/about-banner1.jpg";
import aboutLastBanner from "~/assets/images/about-banner-last.jpg";
const About = () => {
  return (
    <div className={`${styles.aboutContainer} container`}>
      <section className={styles.aboutSection}>
        <div className={styles.wrapper}>
          <div className={styles.imgWrapper}>
            <img src={aboutBanner} alt="" />
          </div>
          <div className={styles.contentWrapper}>
            <h2>We're a group of foodies</h2>
            <p>
              <i>
                <strong className={styles.greyColor}>
                  Simple, Easy Recipes for all.
                </strong>
              </i>{" "}
              Juicy meatballs brisket slammin' baked shoulder. Juicy smoker soy
              sauce burgers brisket, polenta mustard hunk greens. Wine technique
              snack skewers chuck excess. Oil heat slowly, slices natural
              delicious, set aside magic tbsp skillet, bay leaves brown
              centerpiece.
            </p>
          </div>
        </div>
      </section>
      {/* About Us */}
      <section className={styles.aboutSection}>
        <div className={styles.sectionHeader}>
          <h1 className="title">About Us</h1>
          <p className="desc">Everything you need to know !</p>
        </div>
        {/* Information */}
        <div className={styles.information}>
          {aboutContent.map((about, index) => {
            return (
              <div key={index} className={styles.informationWrapper}>
                {/* Content Left */}
                <div className={styles.contentLeft}>
                  <div
                    className={`${styles.contentLeftHeader} ${styles.greyColor}`}
                  >
                    <span>{about.header}</span>
                  </div>
                  <div className={styles.contentLeftTitle}>{about.title}</div>
                  <div className={styles.contentLeftDesc}>{about.desc}</div>
                </div>
                {/* End of Content Left */}

                {/* Content Right */}
                <div className={styles.contentRight}>
                  <img src={about.img} alt="banner" />
                </div>
                {/* End of Content Right */}
              </div>
            );
          })}
        </div>
      </section>
      {/* End Of About Us */}

      {/* Initiatives */}
      <section className={styles.aboutSection}>
        <div className={styles.initiatives}>
          <div className={styles.initiativeWrapper}>
            {/* BackGround Image */}
            <div className={styles.initiativeImg}>
              <img src={aboutLastBanner} alt="last-banner" />
            </div>
            <div className="overlay"></div>
            {/* Content */}
            <div className={styles.initiativeContent}>
              <div className={styles.initiativeTitle}>We Choice</div>
              <div className={styles.initiativeDesc}>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Autem
                voluptatem nam optio qui soluta dignissimos, tempore earum, eius
                non magnam a esse molestiae nobis, tenetur voluptatibus
                necessitatibus maiores quasi eaque?
              </div>
              <div className={styles.initiativeReview}>
                {reviewerContent.map((reviewer, index) => {
                  return (
                    <div key={index} className={styles.initiativeReviewContainer}>
                      <div className={styles.initiativeReviewImg}>
                        <img
                          src={reviewer.img}
                          alt={`reviewer-avatar-${index}`}
                        />
                      </div>
                      <h4 className={styles.initiativeReviewTitle}>
                        {reviewer.name}
                      </h4>
                      <p className={styles.initiativeReviewDesc}>
                        {reviewer.desc}
                      </p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* End Of Initiatives */}
    </div>
  );
};

export default About;
