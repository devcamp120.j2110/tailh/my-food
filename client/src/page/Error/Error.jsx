import React from "react";
import styles from "../../styles/Error.module.css";
import { FaFacebookMessenger } from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";
import { AiOutlineGooglePlus } from "react-icons/ai";
import { BsPinterest } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { navigateToHomePage } from "~/utils";
const Error = () => {
  let navigate = useNavigate();
  
  return (
    <div className={styles.errorPage}>
      <div className={styles.notFound}>
        <h1 className={styles.notFound404}>404</h1>
        <div className={styles.notFoundContent}>
          <h2>OOPS! NOTHING WAS FOUND</h2>
          <p>
            The page you are looking for might have been removed had its name
            changed or is temporarily unavailable
          </p>
          <button
            onClick={() => {
              navigateToHomePage(navigate);
            }}
            className={styles.pushable}
          >
            <span className={styles.front}>Run To Home Page</span>
          </button>
          <div className={styles.notFoundSocial}>
            <ul>
              <li>
                <FaFacebookMessenger />
              </li>
              <li>
                <AiFillInstagram />
              </li>
              <li>
                <AiOutlineGooglePlus />
              </li>
              <li>
                <BsPinterest />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Error;
