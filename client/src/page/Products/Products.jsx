import { useState, useEffect, useRef, useCallback } from "react";
import Card from "../../components/Card";
import StarRating from "../../components/StarRating/StarRating";
import styles from "../../styles/Products.module.css";
import { FaSearch } from "react-icons/fa";
import Pagination from "../../components/Pagination/Pagination";
import { useSelector, useDispatch } from "react-redux";
import { ProductActions } from "~/actions";
import { useNavigate } from "react-router-dom";
import { navigateTo404Page } from "~/utils";

const Products = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const _isMounted = useRef(false);
  const totalItems = useSelector((state) => state.products.counters);
  const [productList, setProductList] = useState([]);
  const [pagination, setPagination] = useState({
    limit: 6,
    page: 1,
  });
  const [tabActive, setTabActive] = useState(0);
  const arrayTabs = ["All Food", "Noodles", "Rice", "Salad", "5 Star"];

  const getInitialization = useCallback(async () => {
    const func = ProductActions.GetPage(pagination);
    const response = await func();
    if (response.status === 200 && response.statusText === "OK") {
      _isMounted.current && setProductList(response.data);
    } else {
      navigateTo404Page(navigate);
    }
  }, [pagination , navigate]);

  const getCountProduct = useCallback(async () => {
    const func = ProductActions.GetCountProduct();
    const response = await func(dispatch);
    if (response.status === 200 && response.statusText === "OK") {
      return;
    } else {
      navigateTo404Page(navigate);
    }
  }, [dispatch , navigate]);

  useEffect(() => {
    _isMounted.current = true;
    return () => {
      _isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    getInitialization();
  }, [getInitialization]);

  useEffect(() => {
    getCountProduct();
  }, [getCountProduct]);

  return (
    <div className={`${styles.productsContainer} container`}>
      {/* Side Bar Desktop*/}
      <div className={styles.sideBar}>
        <h1 className={styles.sideBarTitle}>Categories</h1>
        <div className={styles.listGroupWrapper}>
          {/* Hot Recipe */}
          <ul className={styles.listGroup}>
            <li className={styles.listTitle}>Hot Recipe</li>
            <li>Chinese fried rice</li>
            <li>Ramen noodles</li>
            <li>Pho Viet Nam</li>
            <li>Gimbap Korea</li>
          </ul>
          {/*End Hot Recipe */}

          {/* Best Delicious */}
          <ul className={styles.listGroup}>
            <li className={styles.listTitle}>Best Delicious</li>
            <li>
              <input type="checkbox" id="noodles" />
              <label htmlFor="noodles">Noodles Recipe</label>
            </li>
            <li>
              <input type="checkbox" id="seafood" />
              <label htmlFor="seafood">Seafood Soup</label>
            </li>
            <li>
              <input type="checkbox" id="crab" />
              <label htmlFor="crab">Crab Soup</label>
            </li>
            <li>
              <input type="checkbox" id="hotpot" />
              <label htmlFor="hotpot">Hot Pot</label>
            </li>
          </ul>
          {/* End Best Delicious */}

          {/* Recipe */}
          <ul className={styles.listGroup}>
            <li className={styles.listTitle}>Recipe</li>
            <li>Popular Food</li>
            <li>New Food</li>
            <li>Best Seller</li>
          </ul>
          {/* End Recipe */}

          {/* Search */}
          <ul className={styles.listGroup}>
            <li className={styles.listTitle}>Find Your Food</li>
            <li>
              <div className={styles.searchInputWrapper}>
                <input
                  className={styles.searchInput}
                  type="text"
                  placeholder="Search food here..."
                />
                <FaSearch />
              </div>
            </li>
          </ul>
          {/* End Search */}

          {/* Star Rating */}
          <ul className={styles.listGroup}>
            <li className={styles.listTitle}>Star Rating</li>
            <StarRating value={2} />
          </ul>
          {/* End Star Rating */}
        </div>
      </div>
      {/* End Side Bar Desktop*/}

      {/* Products */}
      <div className={styles.products}>
        {/* Title */}
        <h1 className="title">Products</h1>
        {/* Mobile Navbar */}
        <div className={styles.mobileNavBar}>
          <ul>
            {arrayTabs.map((ele, index) => {
              const active = index === tabActive ? styles.active : "";
              return (
                <li
                  onClick={() => {
                    setTabActive(index);
                  }}
                  className={`${active}`}
                  key={index}
                >
                  <span>{ele}</span>
                </li>
              );
            })}
          </ul>
        </div>
        {/* End Mobile Navbar */}
        {/* List Card */}
        <div className={styles.cardContainer}>
          <Card products={productList ?? []}></Card>
        </div>
        {/* Pagination */}
        <Pagination
          totalItems={totalItems}
          pagination={pagination}
          setPagination={setPagination}
          style={{ paddingBottom: "2rem" }}
        ></Pagination>
      </div>
      {/* End Products */}
    </div>
  );
};

export default Products;
