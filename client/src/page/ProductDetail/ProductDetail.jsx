import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { ProductActions } from "~/actions";
import { useParams, useNavigate } from "react-router-dom";
import { navigateTo404Page, deepCloneObject } from "~/utils";
import styles from "~/styles/ProductDetail.module.css";
import { useDispatch } from "react-redux";
import Chip from "~/components/Chip";
import ButtonQuantity from "~/components/ButtonQuantity";
import Skeleton from "~/components/Skeleton";

const ProductDetail = () => {
  let { productId } = useParams();
  let navigate = useNavigate();
  const dispatch = useDispatch();
  // Set Up State Here
  const _isMounted = useRef(false);
  const [product, setProduct] = useState(null);
  const [tabActive, setTabActive] = useState(0);
  const [mainImg, setMainImg] = useState(null);
  const getInitialization = useCallback(async () => {
    const func = ProductActions.GetSingle(productId);
    const response = await func(dispatch);
    if (response.status === 200 && response.statusText === "OK") {
      _isMounted.current && setProduct(response.data);
    } else {
      navigateTo404Page(navigate);
    }
  }, [productId, navigate, dispatch]);

  const onImgClickChangeMainImg = (paramImg, index) => {
    setMainImg(paramImg);
    setTabActive(index);
  };

  const renderArraySubImg = () => {
    const temp = deepCloneObject(product);
    const mergeArray = product && [temp?.img, ...temp?.galleryImg];
    return mergeArray?.map((img, index) => {
      const active = index === tabActive ? styles.active : "";
      return (
        <div
          onClick={() => {
            onImgClickChangeMainImg(img, index);
          }}
          key={img}
          className={`${styles.subImgWrapper} ${active}`}
        >
          <img src={img} alt="img-sub-detail" />
        </div>
      );
    });
  };

  useEffect(() => {
    _isMounted.current = true;
    return () => {
      _isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    getInitialization();
  }, [getInitialization]);

  const renderBody = () => {
    return (
      <>
        {/* Content Left */}
        <div className={styles.contentLeft}>
          <div className={styles.imgWrapper}>
            <div className={styles.mainImg}>
              <img src={mainImg ?? product?.img} alt="main-img" />
            </div>
            <div className={styles.arraySubImg}>
              {/* Array Img Select */}
              {renderArraySubImg()}
            </div>
          </div>
        </div>
        {/* End Of Content Left */}

        {/* Content Right */}
        <div className={styles.contentRight}>
          {/* Product Title */}
          <h1 className={styles.title}>{product?.title}</h1>
          {/* Product  Desc*/}
          <h1 className={styles.desc}>{product?.desc}</h1>
          {/* Product Type */}
          <div className={styles.categories}>
            {product?.categories &&
              product?.categories.map((cate, index) => {
                return <Chip key={index}>{cate}</Chip>;
              })}
          </div>
          {/* Button Inc/Dec group */}
          <div className={styles.buttonGroup}>
            <div className={styles.buttonQuantity}>
              <ButtonQuantity type="increase" />
              <h3 className={`${styles.quantity} disableSelect`}>1</h3>
              <ButtonQuantity type="decrease" />
            </div>
            {/* Button Add To Card */}
            <button className={styles.addToCardButton}>Add To Cart</button>
          </div>
        </div>
        {/* End Of Content Right */}
      </>
    );
  };

  return (
    <div className={`${styles.productDetailContainer} container`}>
      <div className={styles.productDetailWrapper}>
        {product ? renderBody() : Loading()}
      </div>
    </div>
  );
};

// Loading
function Loading() {
  return (
    <Fragment>
      {/* Content Left */}
      <div className={styles.contentLeft}>
        <div className={styles.imgWrapper}>
          <div style={{ marginBottom: "1rem" }} className={styles.mainImg}>
            <Skeleton className="fullView" />
          </div>
          <div className="bor-rad-10" style={{ height: "120px" }}>
            <Skeleton className="fullView" />
          </div>
        </div>
      </div>
      {/* End Of Content Left */}

      {/* Content Right */}
      <div className={styles.contentRight}>
        {/* Product Title */}
        <Skeleton
          style={{ marginBottom: "0.5rem" }}
          className="fullView title"
        />
        {/* Product  Desc*/}
        <Skeleton className="fullView paragraph" />
        {/* Product Type */}
        <Skeleton className="fullView button" />
        {/* Button Inc/Dec group */}
        <Skeleton className="fullView button" />
      </div>
      {/* End Of Content Right */}
    </Fragment>
  );
}

export default ProductDetail;
