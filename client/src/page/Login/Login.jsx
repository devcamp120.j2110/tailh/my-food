import React from "react";
import styles from "../../styles/Login.module.css";
import MainImg from "../../assets/images/login-img.png";
// Icon
import { FcGoogle } from "react-icons/fc";
import { FaFacebookF } from "react-icons/fa";
import { MdFastfood } from "react-icons/md";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <div className={styles.container}>
      <div className={styles.wrapperContent}>
        {/* Content Left */}
        <div className={styles.contentLeft}>
          <img src={MainImg} alt="login-img" />
        </div>
        {/* End Content Left */}

        {/* Content Right */}
        <div className={styles.contentRight}>
          {/* Login Header */}
          <div className={styles.loginHeader}>
            <h1 className={styles.title}>
              Login to <MdFastfood /> <span>My Food</span>
            </h1>
            <p className={styles.decs}>
              Welcome back! Login with your data that you entered during
              registration.
            </p>
          </div>
          {/* Buttom Google Facebook */}
          <button className={styles.button}>
            <FcGoogle />
            Login with Google
          </button>
          <button className={`${styles.button} ${styles.buttonFacebook}`}>
            <FaFacebookF />
            Login with Facebook
          </button>
          {/* Separated */}
          <div className={styles.separated}>
            <div className={styles.divider}></div>
            <h4 className={styles.separatedContent}>OR</h4>
          </div>
          {/* Login Form */}
          <div className={styles.loginForm}>
            <div className={styles.formGroup}>
              <label htmlFor="login-email">Email*</label>
              <input
                id="login-email"
                type="text"
                placeholder="Enter your email"
              />
            </div>
            <div className={styles.formGroup}>
              <label htmlFor="login-password">Password*</label>
              <input
                id="login-password"
                type="text"
                placeholder="Enter your password"
              />
            </div>
            <div className={styles.formGroup}>
              <button className={styles.button}>Login</button>
            </div>
          </div>
          {/* Login Footer */}
          <div className={styles.loginFooter}>
            <p>
              Don't have an account? <Link to={"/sign-up"}>Sign Up</Link>
            </p>
            <p>
              <a href="/login">Forgot password?</a>
            </p>
          </div>
        </div>
        {/* End Content Right */}
      </div>
    </div>
  );
};

export default Login;
