import Card from "../../components/Card";
import Slider from "../../components/Slider";
import styles from "../../styles/Home.module.css";
import { useNavigate } from "react-router-dom";
import { navigateTo404Page } from "~/utils";
import { useEffect, useMemo, useState, useCallback, useRef } from "react";
import { gallary } from "../../utils";
import { ProductActions } from "~/actions";

const Home = () => {
  const _isMounted = useRef(false);
  let navigate = useNavigate();
  const [productList, setProductList] = useState(null);
  const pagination = useMemo(() => {
    return {
      limit: 4,
      page: 1,
    };
  }, []);

  const getInitialization = useCallback(async () => {
    const func = ProductActions.GetPage(pagination);
    const response = await func();
    if (_isMounted.current && response) {
      if (response.status === 200 && response.statusText === "OK") {
        setProductList(response.data);
      } else {
        navigateTo404Page(navigate);
      }
    }
  }, [pagination, navigate]);

  useEffect(() => {
    _isMounted.current = true;
    return () => {
      _isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    if (_isMounted.current) {
      getInitialization();
    }
  }, [getInitialization]);

  const onBtnViewAllClick = () => {
    navigate("products");
  };

  return (
    <div className={`${styles.homeContainer} container`}>
      {/* Slider */}
      <Slider />
      {/* Products */}
      <section>
        <h1 className="title">New Arrivals</h1>
        <p className="desc">Better Food, Better Mood</p>
        {/* List Card */}
        <div className={styles.cardContainer}>
          {productList ? (
            <Card products={productList}></Card>
          ) : (
            <Card.Loading />
          )}
        </div>
        <button onClick={onBtnViewAllClick} className={styles.viewAllButton}>
          View All
        </button>
      </section>
      {/* End Products */}
      {/* Gallary */}
      <section>
        <h1 className="title">Gallary Food</h1>
        <p className="desc">Eat , Drink , Live</p>
        <div className={styles.gallary}>
          {gallary.map((img, index) => {
            return (
              <div key={index} className={styles.gallaryImg}>
                <img src={img} alt="gallary" />
              </div>
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default Home;
