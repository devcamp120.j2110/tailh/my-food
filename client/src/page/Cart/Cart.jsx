import React from "react";
import styles from "../../styles/Cart.module.css";
import TestImg from "../../assets/images/test-food.png";
import {
  AiFillPlusSquare,
  AiFillMinusSquare,
  AiOutlineDelete,
  AiOutlineEdit,
} from "react-icons/ai";
import { MdLocationOn, MdDeliveryDining } from "react-icons/md";
import { ImTicket } from "react-icons/im";
import { RiExchangeDollarFill } from "react-icons/ri";

const Cart = () => {
  return (
    <div className={`${styles.cartContainer} container`}>
      {/* Cart List */}
      <div className={styles.cartList}>
        {/* Cart Item */}
        <div className={styles.cartItem}>
          {/* Img */}
          <div className={styles.imgWrapper}>
            <img src={TestImg} alt="cart-img" />
          </div>
          {/* Product Info */}
          <div className={styles.productInfo}>
            <p className={styles.productDesc}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
            </p>
            <p className={styles.productTagName}>Noodles</p>
          </div>
          {/* Price */}
          <div className={styles.price}>$300</div>
          {/* Quantity */}
          <div className={styles.quantity}>
            <AiFillPlusSquare />
            {1}
            <AiFillMinusSquare />
          </div>
          {/* Total */}
          <div className={styles.total}>$300</div>
          {/* Delete Button */}
          <div className={styles.deleteBtn}>
            <AiOutlineDelete />
          </div>
        </div>
        {/* Cart Item */}
        <div className={styles.cartItem}>
          {/* Img */}
          <div className={styles.imgWrapper}>
            <img src={TestImg} alt="cart-img" />
          </div>
          {/* Product Info */}
          <div className={styles.productInfo}>
            <p className={styles.productDesc}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
            </p>
            <p className={styles.productTagName}>Noodles</p>
          </div>
          {/* Price */}
          <div className={styles.price}>$300</div>
          {/* Quantity */}
          <div className={styles.quantity}>
            <AiFillPlusSquare />
            {1}
            <AiFillMinusSquare />
          </div>
          {/* Total */}
          <div className={styles.total}>$300</div>
          {/* Delete Button */}
          <div className={styles.deleteBtn}>
            <AiOutlineDelete />
          </div>
        </div>
        {/* Cart Item */}
        <div className={styles.cartItem}>
          {/* Img */}
          <div className={styles.imgWrapper}>
            <img src={TestImg} alt="cart-img" />
          </div>
          {/* Product Info */}
          <div className={styles.productInfo}>
            <p className={styles.productDesc}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
            </p>
            <p className={styles.productTagName}>Noodles</p>
          </div>
          {/* Price */}
          <div className={styles.price}>$300</div>
          {/* Quantity */}
          <div className={styles.quantity}>
            <AiFillPlusSquare />
            {1}
            <AiFillMinusSquare />
          </div>
          {/* Total */}
          <div className={styles.total}>$300</div>
          {/* Delete Button */}
          <div className={styles.deleteBtn}>
            <AiOutlineDelete />
          </div>
        </div>
      </div>
      {/* End Cart List */}
      {/* Cart Summary */}
      <div className={styles.cartSummary}>
        {/* Cart Summary Item -- Address to receive */}
        <div className={`${styles.userAddress} ${styles.cartSummaryItem}`}>
          <h4 className={styles.cartSummaryTitle}>
            <MdLocationOn /> Address to receive
          </h4>
          <article className={styles.userAddressArticle}>
            <p className={styles.userInfo}>
              Hữu Tài - 0904696969{" "}
              <span>
                <AiOutlineEdit />
              </span>
            </p>
            <p className={styles.addressInfo}>
              22 Đường số 10, phường Sơn Kỳ , quận Tân Bình, Hồ Chí Minh{" "}
            </p>
          </article>
        </div>
        {/* Cart Summary Item -- Voucher */}
        <div className={`${styles.cartSummaryItem} ${styles.voucher}`}>
          <h4 className={styles.cartSummaryTitle}>
            <ImTicket /> Voucher
          </h4>
          <button>Pick</button>
        </div>
        {/* Cart Summary Item -- Express */}
        <div className={`${styles.cartSummaryItem} ${styles.express}`}>
          <h4 className={styles.cartSummaryTitle}>
            <MdDeliveryDining /> Express
          </h4>
          <button>Pick</button>
        </div>
        {/* Cart Summary Item -- Order Info */}
        <div className={`${styles.cartSummaryItem} ${styles.orderInfo}`}>
          <h4 className={styles.cartSummaryTitle}>
            <RiExchangeDollarFill /> Order Information
          </h4>
          {/* Order Info Content */}
          <div className={styles.cartOrderInfoContent}>
            {/* Order Cost */}
            <div>
              <label>Order Total:</label>
              <span>$75000</span>
            </div>
            {/* Express Cost */}
            <div>
              <label>Express Cost:</label>
              <span>0</span>
            </div>
            {/* Another Cost */}
            <div>
              <label>Exchange Cost:</label>
              <span>0</span>
            </div>
            {/* Discount */}
            <div>
              <label>Discount:</label>
              <span>$5000</span>
            </div>
          </div>
          {/* End Order Info Content */}

          {/* Amount */}
          <div className={styles.cartOrderInfoAmount}>
            <label>Amount</label>
            <span>$75000</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
