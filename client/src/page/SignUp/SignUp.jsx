import React from "react";
import styles from "../../styles/SignUp.module.css";
import SignUpImg from "../../assets/images/sign-up.png";
import { Link } from "react-router-dom";

// Icon
import { GiMeat } from "react-icons/gi";

const SignUp = () => {
  return (
    <div className={styles.container}>
      <div className={styles.wrapperContent}>
        {/* Left */}
        <div className={styles.contentLeft}>
          <img src={SignUpImg} alt="sign-up-img" />
        </div>
        {/* Right */}
        <div className={styles.contentRight}>
          {/* Header */}
          <div className={styles.signUpHeader}>
            <h1 className={styles.title}>
              Get's <span>started</span>
              <GiMeat />
            </h1>
            <p className={styles.decs}>
              Already have an account?{" "}
              <span>
                <Link to={"/login"}>Login</Link>
              </span>
            </p>
          </div>
          {/* End Header */}
          {/* Form */}
          <div className={styles.signUpForm}>
            {/* Name */}
            <div className={styles.formGroup}>
              <label htmlFor="signup-name">Name *</label>
              <input
                id="signup-name"
                type="text"
                placeholder="Enter your name"
              />
            </div>
            {/* Email */}
            <div className={styles.formGroup}>
              <label htmlFor="signup-email">Email *</label>
              <input
                id="signup-email"
                type="text"
                placeholder="Enter your email"
              />
            </div>
            {/* Phone */}
            <div className={styles.formGroup}>
              <label htmlFor="signup-phone">Phone *</label>
              <input
                id="signup-phone"
                type="text"
                placeholder="Enter your phone"
              />
            </div>
            {/* Password */}
            <div className={styles.formGroup}>
              <label htmlFor="signup-password">Password *</label>
              <input
                id="signup-password"
                type="text"
                placeholder="Enter your password"
              />
            </div>
            {/* Confirm Password */}
            <div className={styles.formGroup}>
              <label htmlFor="signup-confirm-password">
                Confirm Password *
              </label>
              <input
                id="signup-confirm-password"
                type="text"
                placeholder="Enter your confirm password"
              />
            </div>
          </div>
          {/* End Form */}
          {/* Confirm Policy */}
          <div className={styles.confirmPolicy}>
            <input id="confirm-policy" type="checkbox" />
            <label htmlFor="confirm-policy">
              I agree to Platform's <span>Terms of Service</span> and{" "}
              <span>Privacy Policy</span>
            </label>
          </div>
          {/* End Confirm Policy */}
          {/* Button Register */}
          <button className={styles.button}>Create Account</button>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
